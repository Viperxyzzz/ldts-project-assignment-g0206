## LDTS0206 - Space Invaders

LDTS Space Invaders é uma implementação do clássico Space Invaders, seguindo boas
práticas de programação.
\
\
Uma explicação mais detalhada do jogo pode ser encontrada [aqui](https://gitlab.com/Viperxyzzz/ldts-project-assignment-g0206/-/tree/main/docs)

## Instruções

### Menu inicial
No menu inicial a navegação é feita através da <em>Arrow Up e Arrow Down</em> do teclado. Para selecionar uma das opções basta precionar o <em>Enter</em>.

### Jogo
No jogo o utilizador pode movimentar-se através da <em>Arrow Right e Arrow Left</em>. Para disparar tem que pressionar <em>Arrow up</em> e para sair do jogo basta clicar <em>Escape</em>.

### Final do jogo
No final do jogo apenas podemos pressionar o <em>Enter</em>.

## Screenshots do jogo

### Menu Inicial
![menu inicial](docs/short_readme/menu_inicial.png)

### Game Play
![gameplay](docs/short_readme/gameplay.png)

### Ecrã Final
![ecra final](docs/short_readme/win_screen.png)
