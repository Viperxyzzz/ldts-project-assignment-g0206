## LDTS0206 - Space Invaders

LDTS Space Invaders é uma implementação do clássico Space Invaders, seguindo boas
práticas de programação.

---

## Features

### Features Implementadas

**Elementos**
- Aliens
- Spaceship
- Grupo de aliens
- Balas
- Butões
- UI:
    1. total de balas
- Menus:
    1. Menu Inicial
    2. Menu Game Over

**Lógica**


- Movimento - A spaceship já apresenta movimento aquando da introdução de input.
- Disparar - A spaceship já consegues disparar projeteis.
- Game Over - Existe deteção do game over.
- Colisões - As balas possuem colisões com os aliens.

### Features Planeadas
Todas as features planeadas foram implementadas

---
### DESIGN


- **Problema** - Necessitávamos de um modelo que nos permitisse facilmente inserir elementos, representa-los e expandi-los. 

- **O pattern** - Para tal decidimos utilizar o MVC (model-view-controller), onde o model guarda a informação, view representa a informação o controller altera a informação presente no model.

    **Model**
    
    Modelos usados para os botões

    ![img](images/model_buttons.jpg)

    Modelos usados para os elementos

    ![img](images/model_entity.jpg)

    Modelo usado para os menus

    ![img](images/Model.jpg)

    **View**

    Usado para desenhar a API

    ![img](images/view_api.jpg)

    Usado para desenhar os elementos

    ![img](images/view_game.jpg)
    
    **Controller**

    Controladores usados para as entidades
    
    ![img](images/controller_entity.jpg)

    Controladores usados para os botões

    ![img](images/controller_buttons.jpg)

    Controladores usados para os movimentos

    ![img](images/controller_move.jpg)

    Controlador para o jogo

    ![img](images/controller_game.jpg)

    Controlador para observers

    ![img](images/controller_observer.jpg)

O resto dos controladores é abordado nos design patterns,

### Modos de disparo

- **Problema** - A spaceship irá disparar balas, e a maneira como as dispara poderá ser diferente dependendo das condições do jogo. Por exemplo, se a mesma spaceship ficar sem balas é necessário que esta não dispare mais.
- **O pattern** - Como resposta decidimos usar o ***Strategy pattern***, um <em>behavorial pattern</em> que nos permite defenir algoritmos diferentes para uma mesma classe. O state pattern também podia ser utilizado neste problema, porém o state pattern possui a premissa de os estados poderem alterar-se a si mesmos, algo que nós não queriamos. O command pattern também foi algo
que foi pensado porém nesta situação nós queriamos algo que especificasse a maneira como algo é feito, em vez de criar um objecto para algo que precisa de ser feito.
- **Implementação** -\
    ![img](images/strategypattern.jpg)
    \
    O pattern é utilizado nas classes seguintes : 
    - [BulletStrategy](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/blob/main/src/main/java/Controller/bullet/BulletStrategy.java)
    - [CantShootStrategy](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/blob/main/src/main/java/Controller/bullet/CantShootStrategy.java)
    - [NormalBulletStrategy](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/blob/main/src/main/java/Controller/bullet/NormalBulletStrategy.java)
    - [BulletStrategyObserver](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/blob/main/src/main/java/Controller/observer/BulletStrategyObserver.java)
    - [SpaceshipController](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/blob/main/src/main/java/Controller/entity/SpaceshipController.java)

- **Consequencias** - Com o strategy pattern conseguimos alterar o modo de disparo em <em>runtime</em> e ao mesmo tempo separar o código da spaceship do modo de disparo.

### Viewers em runtime

- **Problema** - Todos os nossos objetos encontram-se copulados numa lista de entities, e nós queriamos poder respresentar visualmente as mesmas do modo mais simples possivel. Para além do mais são criadas novas entities em run-time, e estas também precisam de ser desenhadas
- **O pattern** - Face a isto, tornou-se evidente que a resposta era o ***Factory pattern***, um <em>creational pattern</em> que assenta em criar uma interface que cria um objeto generico, que é implementado por sub-classes.
- **Implementação** - \
    ![img](images/viewerfactory.jpg)
    \
    O pattern é utilizado nas classes seguintes : 
    - [EntityViewerFactory](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/blob/main/src/main/java/View/entity/EntityViewerFactory.java)
    - [GameViewer](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/blob/main/src/main/java/View/states/GameViewer.java)
- **Consequencias** - Torna-se bastante facil adicionar elementos novos e representa-los visualmente, apenas necessitamos de adicionar o método de fabrico à factory. No geral torna o processo de representação visual das entidades muito simples.


### Eventos
- **Problema** - Existem vários eventos em runtime que precisam de ser tratados, a spaceship a disparar, colisões das balas com os aliens e o final do jogo. Nos menus é também necessário saber quando certas teclas são pressionadas para mudar de estado.
- **O pattern** - Como resposta decidimos aplicar o ***Observer pattern***, um <em>behavorial pattern</em> que nos permite estabelecer um mecanismo de subscrição e notificar vários objetos quando certos eventos acontecem.
- **Implementação**- \
    ![img](images/gameeventsobserver.jpg)
    \
    O pattern é utilizado nas classes seguintes (todos os states acabam por utilizar os observers) : 
    - [Observers](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/tree/main/src/main/java/Controller/observer)
    - [States](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/tree/main/src/main/java/Controller/states)
- **Consequencia** - Torna-se bastante simples dar handle aos varios eventos do jogo para além de deixar o código mais modular e fácil de testar.

### Estados do jogo
- **Problema** - Como se tratava de um jogo precisavamos de um menu inicial, de um menu final e o jogo própriamente dito. Era ao mesmo tempo necessário que estes estados podessem trocar entre si.
- **O pattern** - Face isto decidimos usar o ***State pattern***, um <em>behavorial pattern</em> que permite alterar o comportamento de um objeto em run-time, dependendo do seu conteudo.
- **Implementação**- \
    ![img](images/statepattern.jpg)
    \
    O pattern é utilizado nas classes seguintes : 
    - [GameOverState](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/blob/main/src/main/java/Controller/states/GameOverState.java)
    - [MenuState](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/blob/main/src/main/java/Controller/states/MenuState.java)
    - [PlayState](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/blob/main/src/main/java/Controller/states/PlayState.java)
    - [Game](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/blob/main/src/main/java/Controller/Game.java)
- **Consequencias** - Permite-nos ter uma interface com os vários estados e uma fácil permuta dos mesmos. Torna-se também bastante facil adicionar estados novos.


### Butões
- **Problema** - Necessitavamos de uma maneira de fazer os butões do menu sem repetir muito código, dado que a diferença entre cada um era apenas a função que executava. 
- **O pattern** - Para tal decidimos utilizar o ***Command pattern***, um <em>behavorial pattern</em> que nos permite extrair todos os comandos diferentes para classes diferentes.
- **Implementação**- \
    ![img](images/commandpattern.jpg)
    \
    O pattern é utilizado nas classes seguintes : 
    - [ButtonCommand](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/blob/main/src/main/java/Controller/buttons/ButtonCommand.java)
    - [ExitCommand](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/blob/main/src/main/java/Controller/buttons/ExitCommand.java) 
    - [PlayCommand](https://github.com/FEUP-LDTS-2021/ldts-project-assignment-g0206/blob/main/src/main/java/Controller/buttons/PlayCommand.java)
- **Consequencias**  - Ao usarmos o command pattern podemos separar os botões daquilo que estes vão fazer permitindo <em>clean code</em> e uma facil criação dos mesmos.
------


#### Code smells conhecidos e sugestões para refactoring dos mesmos

### Switch Statements
- Possuimos vários switch statments, sendo os mais notórios na Factory dos viewers e no handler de input.
- Com o handler de input acaba por ser dificil dar refactor, o mais próximo disse seria aplicar o extract method, metendo o código noutras funções e depois o move method para mover as funções para os respetivos controllers.
- Com a factory podiamos aplicar polimorfismo, abastraindo a classe viewerfactory. Em cada entity implementavamos a função getEntityViewer e no loop chamavamo-la.
### Large Class
- A class playstate está a fazer demasiadas coisas.
- Para a parte do construtor, o build pattern podia ser utilizado, por conveniencia foi usado o extract method.
- A classe tem vários metodos que talvez podessem ser transferidos para outras classes.
### Long Parameter List
- A maior parte dos metodos das entities sofre de long parameter list

---
### TESTING

#### Tests Coverage
![img](images/tabelatestes.png)

#### Pit Test Coverage
![img](images/pitest.png)
---
### SELF-EVALUATION

- Pedro Nunes: 65%
- Tomás Macedo: 35%
