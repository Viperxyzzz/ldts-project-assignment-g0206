package Controller.entity;

import Controller.bullet.BulletStrategy;
import Model.entity.Spaceship;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SpaceshipControllerTest {
    private Spaceship spaceship;
    private SpaceshipController spaceshipController;
    private BulletStrategy bulletStrategy;

    @BeforeEach
    void setUp(){
        this.spaceship = Mockito.mock(Spaceship.class,Mockito.CALLS_REAL_METHODS);
        this.bulletStrategy = Mockito.mock(BulletStrategy.class);
        this.spaceshipController = new SpaceshipController(spaceship,bulletStrategy);
    }

    @Test
    void shootTest(){
        spaceshipController.shoot();
        Mockito.verify(bulletStrategy,Mockito.times(1)).shoot(spaceship);

    }

}
