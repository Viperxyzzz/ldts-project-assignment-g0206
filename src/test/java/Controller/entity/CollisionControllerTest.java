package Controller.entity;

import Model.entity.Alien;
import Model.entity.Bullet;
import Model.entity.Entity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class CollisionControllerTest {
    private List<Alien> alienList;
    private List<Bullet> bulletList;
    private List<Alien> destroyedAliens;
    private List<Bullet> destroyedBullets;
    private CollisionController collisionController;
    @BeforeEach
    void setUp(){
        this.collisionController = new CollisionController();
    }
    @Test
    void collisionTest(){
        Entity lhsEntity = new Entity(10,10,100,100,"blue");
        Entity rhsEntity = new Entity(20,20,100,100,"blue");
        Entity collisionRhsEntity = new Entity(20,10,50,100,"blue");
        assertFalse(this.collisionController.collision(lhsEntity,rhsEntity));
        assertTrue(this.collisionController.collision(lhsEntity,collisionRhsEntity));
    }

    @Test
    void canMoveTest(){
        Entity entity = new Entity(10,10,10,10,"blue");
        assertTrue(this.collisionController.canMove(entity));
        entity.setPosition(-1,20);
        assertFalse(this.collisionController.canMove(entity));
        entity.setPosition(20,51);
        assertFalse(this.collisionController.canMove(entity));
        entity.setPosition(20,-1);
        assertFalse(this.collisionController.canMove(entity));
        entity.setPosition(71,20);
        assertFalse(this.collisionController.canMove(entity));
    }

    @Test
    void checkBoundariesTest(){
        List<Bullet> bulletList = new ArrayList<Bullet>();
        bulletList.add(new Bullet(10,-3,10,10,"blue",10,10));
        bulletList.add(new Bullet(10,10,10,10,"blue",10,10));
        this.collisionController.setBulletList(bulletList);
        this.collisionController.checkBoundaries();
        assertEquals(this.collisionController.getDestroyedBullets().size(),1);
    }


}
