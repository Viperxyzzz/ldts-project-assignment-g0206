package Controller.move;

import Model.entity.MovableEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MoveTest {
    private MovableEntity entity;
    @BeforeEach
    void setUp(){
        entity = new MovableEntity(5,5,10,10,"blue",1);
    }
    @Test
    void MoveRightCommandTest(){
        MoveRightCommand moveRightCommand = new MoveRightCommand(entity);
        int result = entity.getX() + entity.getSpeed();
        moveRightCommand.execute();
        assertEquals(result,entity.getX());
    }
    @Test
    void MoveLeftCommandTest(){
        MoveLeftCommand moveLeftCommand = new MoveLeftCommand(entity);
        int result = entity.getX() - entity.getSpeed();
        moveLeftCommand.execute();
        assertEquals(result,entity.getX());
    }
    @Test
    void MoveInvokerTest(){
        MoveLeftCommand moveLeftCommand = new MoveLeftCommand(entity);
        MoveLeftCommand moveLeftCommand2 = new MoveLeftCommand(entity);
        MoveRightCommand moveRightCommand = new MoveRightCommand(entity);
        int result = entity.getX() - entity.getSpeed();
        List<Move> moveList = new ArrayList<Move>();
        moveList.add(moveLeftCommand);
        moveList.add(moveRightCommand);
        moveList.add(moveLeftCommand2);
        MoveInvoker moveInvoker = new MoveInvoker(moveList);
        moveInvoker.applyMoves();
        assertEquals(result,entity.getX());
        
    }
}
