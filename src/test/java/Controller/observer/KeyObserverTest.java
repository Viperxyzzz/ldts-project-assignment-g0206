package Controller.observer;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class KeyObserverTest {
    private KeySubject keySubject;
    private KeyObserver keyObserver;
    @BeforeEach
    void setUp(){
        this.keySubject = new KeySubject();
        this.keyObserver = new KeyObserver();
        keySubject.addObserver(keyObserver);
    }
    @Test
    void KeyObserverTest(){
        assertEquals(this.keyObserver.getKeyPressed(),false);
        keySubject.notifyObservers();
        assertEquals(this.keyObserver.getKeyPressed(),true);
    }
}
