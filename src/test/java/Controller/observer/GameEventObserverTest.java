package Controller.observer;

import Controller.entity.SpaceshipController;
import Controller.Game;
import Controller.bullet.BulletStrategy;
import Controller.bullet.CantShootStrategy;
import Controller.bullet.NormalBulletStrategy;
import Model.states.GameModel;
import Model.uibar.UIBar;
import Model.entity.Spaceship;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GameEventObserverTest {
    private Game game;
    private GameEventsManager gameEventsManager;
    @BeforeEach
    void setUp(){

        game = Mockito.mock(Game.class);
        gameEventsManager = new GameEventsManager();
    }
    @Test
    void GameOverObserverTest(){
        GameModel gameModel = new GameModel();
        GameOverObserver gameOverObserver = new GameOverObserver(gameModel);
        gameEventsManager.subscribe("gameOver",gameOverObserver);
        assertEquals(gameModel.isRunning(),true);
        gameEventsManager.notify("gameOver",0);
        assertEquals(gameModel.isRunning(),false);
    }
    @Test
    void BulletStrategyObserverTest(){
        Spaceship spaceship = Mockito.mock(Spaceship.class,Mockito.CALLS_REAL_METHODS);
        BulletStrategy bulletStrategy = Mockito.mock(BulletStrategy.class);
        SpaceshipController spaceshipController = new SpaceshipController(spaceship,bulletStrategy);
        BulletStrategyObserver bulletStrategyObserver = new BulletStrategyObserver(spaceshipController);
        bulletStrategyObserver.update(0);
        assertTrue(spaceshipController.getBulletStrategy() instanceof CantShootStrategy);
        bulletStrategyObserver.update(1);
        assertTrue(spaceshipController.getBulletStrategy() instanceof NormalBulletStrategy);
    }
    @Test
    void SpaceshipBulletObserverTest(){
        Spaceship spaceship = Mockito.mock(Spaceship.class,Mockito.CALLS_REAL_METHODS);
        SpaceshipBulletObserver spaceshipBulletObserver = new SpaceshipBulletObserver(spaceship);
        gameEventsManager.subscribe("SpaceshipBulletObserverTest",spaceshipBulletObserver);
        int result = 10;
        gameEventsManager.notify("SpaceshipBulletObserverTest",result);
        assertEquals(result,spaceship.getTotalBullets());
    }

    @Test
    void UIBulletObserverTest(){
        UIBar uiBar = Mockito.mock(UIBar.class,Mockito.CALLS_REAL_METHODS);
        UIBulletObserver uiBulletObserver = new UIBulletObserver(uiBar);
        gameEventsManager.subscribe("UIBulletObserverTest",uiBulletObserver);
        int result = 10;
        gameEventsManager.notify("UIBulletObserverTest",result);
        assertEquals(result,uiBar.getTotalBullets());
    }


}
