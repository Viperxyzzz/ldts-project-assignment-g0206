package Controller.states;

import Controller.Game;
import Controller.observer.KeyObserver;
import Model.states.GameOverModel;
import View.API.GUI;
import com.googlecode.lanterna.input.KeyType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;

public class GameOverStateTest {
    private GUI gui;
    private Game game;
    private GameOverModel gameOverModel;
    private GameOverState gameOverState;
    private KeyObserver keyObserver;
    @BeforeEach
    void setUp(){
        gui = Mockito.mock(GUI.class);
        game = Mockito.mock(Game.class);
        gameOverModel = Mockito.mock(GameOverModel.class);
        gameOverState = new GameOverState(game,gui,true);
        keyObserver = Mockito.mock(KeyObserver.class);
    }
    @Test
    void runTest() throws IOException, InterruptedException {
        Mockito.when(gui.getKeyPolling()).thenReturn(KeyType.ArrowLeft);
        gameOverState.setKeyObserver(keyObserver);
        Mockito.when(keyObserver.getKeyPressed()).thenReturn(true);
        gameOverState.run();

        Mockito.verify(gui,Mockito.times(1)).clearScreen();
        Mockito.verify(gui,Mockito.times(1)).refresh();
        Mockito.verify(keyObserver,Mockito.times(1)).getKeyPressed();
    }
}
