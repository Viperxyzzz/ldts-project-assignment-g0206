package Controller.states;

import Controller.Game;
import Controller.observer.KeyObserver;
import Model.states.MenuModel;
import View.API.GUI;
import com.googlecode.lanterna.input.KeyType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;

public class MenuStateTest {
    private GUI gui;
    private Game game;
    private MenuModel menuModel;
    private MenuState menuState;
    private KeyObserver keyObserver;
    @BeforeEach
    void setUp(){
        gui = Mockito.mock(GUI.class);
        game = Mockito.mock(Game.class);
        menuModel = Mockito.mock(MenuModel.class);
        menuState = new MenuState(game,gui);
        keyObserver = Mockito.mock(KeyObserver.class);
    }

    @Test
    void runTest() throws IOException, InterruptedException {
        Mockito.when(gui.getKey()).thenReturn(KeyType.ArrowLeft);
        menuState.setKeyObserver(keyObserver);
        Mockito.when(keyObserver.getKeyPressed()).thenReturn(true);
        menuState.run();

        Mockito.verify(gui,Mockito.times(1)).clearScreen();
        //draw
        Mockito.verify(gui,Mockito.times(1)).refresh();
        Mockito.verify(keyObserver,Mockito.times(1)).getKeyPressed();
    }
}
