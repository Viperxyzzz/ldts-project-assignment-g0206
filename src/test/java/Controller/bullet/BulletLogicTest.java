package Controller.bullet;

import Model.entity.Bullet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class BulletLogicTest {
    private List<Bullet> bulletList;
    private BulletLogic bulletLogic;
    @BeforeEach
    void setUp(){
        Bullet bullet = Mockito.mock(Bullet.class);
        Mockito.when(bullet.getIsMoving()).thenReturn(true);
        Mockito.when(bullet.getX()).thenReturn(10);
        Mockito.when(bullet.getY()).thenReturn(5);
        Mockito.when(bullet.getSpeed()).thenReturn(1);
        bulletList = new ArrayList<Bullet>();
        bulletLogic = new BulletLogic();
        bulletLogic.addBullet(bullet);
    }
    @Test
    void updateBulletsTest(){
        bulletLogic.updateBullets();
        for(Bullet bullet : bulletLogic.getBulletList()){
            Mockito.verify(bullet,Mockito.times(1)).setPosition(bullet.getX(),bullet.getY() - bullet.getSpeed());
        }
    }

}
