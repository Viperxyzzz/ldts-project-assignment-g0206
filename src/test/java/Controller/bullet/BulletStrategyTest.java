package Controller.bullet;

import Controller.entity.SpaceshipController;
import Model.entity.Bullet;
import Model.entity.Spaceship;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BulletStrategyTest {
    private Spaceship spaceship;
    private SpaceshipController spaceshipController;
    @BeforeEach
    void setUp(){
        BulletStrategy bulletStrategy = Mockito.mock(BulletStrategy.class);
        this.spaceship = Mockito.mock(Spaceship.class,Mockito.CALLS_REAL_METHODS);
        this.spaceshipController = new SpaceshipController(spaceship,bulletStrategy);
    }

    @Test
    void normalShootTest(){
        int totalBullets = spaceship.getTotalBullets() -1 ;
        NormalBulletStrategy normalBulletStrategy = new NormalBulletStrategy();
        this.spaceshipController.setBulletStrategy(normalBulletStrategy);
        Bullet bullet = spaceshipController.shoot();
        assertEquals(bullet.getIsMoving(),true);
        assertEquals(spaceship.getTotalBullets(),totalBullets);
    }
    @Test
    void cantShootTest(){
        int totalBullets = spaceship.getTotalBullets();
        CantShootStrategy cantShootStrategy = new CantShootStrategy();
        this.spaceshipController.setBulletStrategy(cantShootStrategy);
        Bullet bullet = spaceshipController.shoot();
        assertEquals(bullet,null);
        assertEquals(spaceship.getTotalBullets(),totalBullets);


    }


}
