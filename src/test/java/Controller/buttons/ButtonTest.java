package Controller.buttons;

import Controller.Game;
import Controller.states.PlayState;
import View.API.GUI;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;

public class ButtonTest {
    private GUI gui;
    private Game game;
    @BeforeEach
    void setUp(){
        this.gui = Mockito.mock(GUI.class);
        this.game = Mockito.mock(Game.class);
    }
    @Test
    void playCommandTest(){
        PlayCommand playCommand = new PlayCommand(this.game,this.gui);
        playCommand.execute();
        Mockito.verify(game,Mockito.times(1)).setState(new PlayState(this.game,this.gui));
    }
    @Test
    void exitCommandTest() throws IOException {
        ExitCommand exitCommand = new ExitCommand(game,this.gui);
        exitCommand.execute();
        Mockito.verify(gui,Mockito.times(1)).close();
        Mockito.verify(game,Mockito.times(1)).stopRunning();
    }
}