package Model.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

public class SpaceshipTest {

    @Spy
    Spaceship spaceship = new Spaceship(10, 20, 30, 40, "blue", 50);


    @Test
    public void spaceshipNotNullTest(){
        boolean res=false;
        if(spaceship != null){res=true;}
        Assertions.assertEquals(true, res);
    }

    @Test
    public void spaceshipXAttributeTest(){
        Assertions.assertEquals(10, spaceship.getX());
    }

    @Test
    public void spaceshipYAttributeTest(){
        Assertions.assertEquals(20, spaceship.getY());
    }

    @Test
    public void spaceshipWidthAttributeTest(){
        Assertions.assertEquals(30, spaceship.getWidth());
    }

    @Test
    public void spaceshipHeightAttributeTest(){
        Assertions.assertEquals(40, spaceship.getHeight());
    }

    @Test
    public void spaceshipColorAttributeTest(){
        Assertions.assertEquals("blue", spaceship.getColor());
    }

    @Test
    public void spaceshipSpeedAttributeTest(){
        Assertions.assertEquals(50, spaceship.getSpeed());
    }
}
