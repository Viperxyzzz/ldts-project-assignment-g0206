package Model.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

public class AlienGroupTest {

    @Spy AlienGroup test = new AlienGroup(10, 20, 30, 40, "blue", 32);


    @Test
    public void alienGroupNotNullTest(){
        boolean res=false;
        if(test != null){res=true;}
        Assertions.assertEquals(true, res);
    }

    @Test
    public void alienGroupXAttributeTest(){
        Assertions.assertEquals(10, test.getX());
    }

    @Test
    public void alienGroupYAttributeTest(){
        Assertions.assertEquals(20, test.getY());
    }

    @Test
    public void alienGroupWidthAttributeTest(){
        Assertions.assertEquals(30, test.getWidth());
    }

    @Test
    public void alienGroupHeightAttributeTest(){
        Assertions.assertEquals(40, test.getHeight());
    }

    @Test
    public void alienGroupColorAttributeTest(){
        Assertions.assertEquals("blue", test.getColor());
    }

    @Test
    public void alienGroupNumberOfAliensAttributeTest(){Assertions.assertEquals(32, test.getTotalAliens());}


}
