
import Model.entity.Asteroid;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

public class AsteroidTest {

    @Spy
    Asteroid asteroid = new Asteroid(10, 20, 30, 40, "blue", 50);


    @Test
    public void asteroidNotNullTest(){
        boolean res=false;
        if(asteroid != null){res=true;}
        Assertions.assertEquals(true, res);
    }

    @Test
    public void asteroidXAttributeTest(){
        Assertions.assertEquals(10, asteroid.getX());
    }

    @Test
    public void asteroidYAttributeTest(){
        Assertions.assertEquals(20, asteroid.getY());
    }

    @Test
    public void asteroidWidthAttributeTest(){
        Assertions.assertEquals(30, asteroid.getWidth());
    }

    @Test
    public void asteroidHeightAttributeTest(){
        Assertions.assertEquals(40, asteroid.getHeight());
    }

    @Test
    public void asteroidColorAttributeTest(){
        Assertions.assertEquals("blue", asteroid.getColor());
    }

    @Test
    public void asteroidSpeedAttributeTest(){
        Assertions.assertEquals(50, asteroid.getSpeed());
    }




}
