package Model.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

public class BulletTest {

    @Spy Bullet test = new Bullet(10, 20, 30, 40, "blue", 50, 60);


    @Test
    public void asteroidNotNullTest(){
        boolean res=false;
        if(test != null){res=true;}
        Assertions.assertEquals(true, res);
    }

    @Test
    public void bulletXAttributeTest(){
        Assertions.assertEquals(10, test.getX());
    }

    @Test
    public void bulletYAttributeTest(){
        Assertions.assertEquals(20, test.getY());
    }

    @Test
    public void bulletWidthAttributeTest(){
        Assertions.assertEquals(30, test.getWidth());
    }

    @Test
    public void bulletHeightAttributeTest(){
        Assertions.assertEquals(40, test.getHeight());
    }

    @Test
    public void bulletColorAttributeTest(){
        Assertions.assertEquals("blue", test.getColor());
    }

    @Test
    public void bulletSpeedAttributeTest(){
        Assertions.assertEquals(50, test.getSpeed());
    }

}
