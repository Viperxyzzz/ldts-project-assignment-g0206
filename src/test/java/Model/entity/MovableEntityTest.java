
import Model.entity.MovableEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

public class MovableEntityTest {

    @Spy
    MovableEntity movableEntity = new MovableEntity(10, 20, 30, 40, "blue", 50);


    @Test
    public void movableEntityNotNullTest(){
        boolean res=false;
        if(movableEntity != null){res=true;}
        Assertions.assertEquals(true, res);
    }

    @Test
    public void movableEntityXAttributeTest(){
        Assertions.assertEquals(10, movableEntity.getX());
    }

    @Test
    public void movableEntityYAttributeTest(){
        Assertions.assertEquals(20, movableEntity.getY());
    }

    @Test
    public void movableEntityWidthAttributeTest(){
        Assertions.assertEquals(30, movableEntity.getWidth());
    }

    @Test
    public void movableEntityHeightAttributeTest(){
        Assertions.assertEquals(40, movableEntity.getHeight());
    }

    @Test
    public void movableEntityColorAttributeTest(){
        Assertions.assertEquals("blue", movableEntity.getColor());
    }

    @Test
    public void movableEntitySpeedAttributeTest(){
        Assertions.assertEquals(50, movableEntity.getSpeed());
    }
}
