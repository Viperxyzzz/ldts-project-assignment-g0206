
import Model.entity.Entity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

public class EntityTest {

    @Spy
    Entity entity = new Entity(10, 20, 30, 40, "blue");


    @Test
    public void entityNotNullTest(){
        boolean res=false;
        if(entity != null){res=true;}
        Assertions.assertEquals(true, res);
    }

    @Test
    public void entityXAttributeTest(){
        Assertions.assertEquals(10, entity.getX());
    }

    @Test
    public void entityYAttributeTest(){
        Assertions.assertEquals(20, entity.getY());
    }

    @Test
    public void entityWidthAttributeTest(){
        Assertions.assertEquals(30, entity.getWidth());
    }

    @Test
    public void entityHeightAttributeTest(){
        Assertions.assertEquals(40, entity.getHeight());
    }

    @Test
    public void entityColorAttributeTest(){
        Assertions.assertEquals("blue", entity.getColor());
    }
}
