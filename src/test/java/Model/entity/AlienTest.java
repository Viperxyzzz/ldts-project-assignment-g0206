package Model.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;

public class AlienTest {

    @Spy Alien alien = new Alien(10, 20, 30, 40, "blue");


    @Test
    public void alienNotNullTest(){
        boolean res=false;
        if(alien != null){res=true;}
            Assertions.assertEquals(true, res);
    }

    @Test
    public void alienXAttributeTest(){
        Assertions.assertEquals(10, alien.getX());
    }

    @Test
    public void alienYAttributeTest(){
        Assertions.assertEquals(20, alien.getY());
    }

    @Test
    public void alienWidthAttributeTest(){
        Assertions.assertEquals(30, alien.getWidth());
    }

    @Test
    public void alienHeightAttributeTest(){
        Assertions.assertEquals(40, alien.getHeight());
    }

    @Test
    public void alienColorAttributeTest(){
        Assertions.assertEquals("blue", alien.getColor());
    }



}
