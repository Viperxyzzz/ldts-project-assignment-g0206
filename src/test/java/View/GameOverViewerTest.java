package View;

import Controller.Game;
import Model.states.GameOverModel;
import View.API.GUI;
import View.states.GameOverViewer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class GameOverViewerTest {
    private GUI gui;
    private GameOverModel gameOverModel;
    private GameOverViewer gameOverViewer;
    @BeforeEach
    void setUp(){
        gui = Mockito.mock(GUI.class);
        Game game = Mockito.mock((Game.class));
        this.gameOverModel = new GameOverModel(true);
        this.gameOverViewer = new GameOverViewer(this.gui,this.gameOverModel);
    }
    @Test
    void draw(){
        gameOverViewer.draw();
        Mockito.verify(gui,Mockito.times(1)).drawText("blue",gameOverModel.getMessage(),35,10);
        Mockito.verify(gui,Mockito.times(1)).drawText("white",gameOverModel.getBlinkingMessage(),25,30);
    }
}
