package View.button;

import Model.buttons.Button;
import View.API.GUI;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class ButtonViewerTest {
    private ButtonViewer buttonViewer;
    private Button button;
    private GUI gui;
    @BeforeEach
    void setUp(){
        gui = Mockito.mock(GUI.class);
        button = Mockito.mock(Button.class,Mockito.CALLS_REAL_METHODS);
        buttonViewer = new ButtonViewer();
    }
    @Test
    void draw(){
        buttonViewer.draw(gui,button);
        int width = button.getWidth();
        int height = button.getHeight();
        int x = button.getX();
        int y = button.getY();

        for(int i = 0; i < height; i++){
            Mockito.verify(gui,Mockito.times(1)).drawLine("red",x,y+i,width);
            if(button.isHovering()){
                if(i == 0 || i == height - 1){
                    Mockito.verify(gui,Mockito.times(1)).drawLine("#FFFF00",x,y + i,width);
                }
                else{
                    Mockito.verify(gui,Mockito.times(1)).drawSquare("#FFFF00",x,y+i);
                    Mockito.verify(gui,Mockito.times(1)).drawSquare("#FFFF00",x + width,y+i);
                }
            }
            if(i == height / 2){
                Mockito.verify(gui,Mockito.times(1)).drawText("white",button.getText(),(width/2 + x),y + i);
            }
        }
    }
}
