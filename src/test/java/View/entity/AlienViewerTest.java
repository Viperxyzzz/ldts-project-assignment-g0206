package View.entity;

import Model.entity.Alien;
import View.API.GUI;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class AlienViewerTest {
    private GUI gui;
    private Alien alien;
    private AlienViewer alienViewer;

    @BeforeEach
    void setUp(){
        alien = new Alien(10, 60 * 1/4,4,3,"blue");
        alienViewer = new AlienViewer();
        gui = Mockito.mock(GUI.class);
    }

    @Test
    void draw(){
        alienViewer.draw(gui,alien);
        int height = alien.getHeight();
        int x = alien.getX();
        int y = alien.getY();
        int width = alien.getWidth();
        String color = alien.getColor();

        for(int i = 0; i < height; i++){
            Mockito.verify(gui,Mockito.times(height)).drawLine(color, x, y, width);
        }

        Mockito.verify(gui,Mockito.times(1)).drawSquare(color, x + width - 1, y + 1);
        Mockito.verify(gui,Mockito.times(1)).drawSquare(color, x + 1, y + 1);
    }


}
