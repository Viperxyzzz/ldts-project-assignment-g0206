package View.entity;

import Model.entity.Entity;
import View.API.GUI;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class SpaceshipViewerTest {
    private GUI gui;
    private Entity entity;
    private SpaceshipViewer spaceshipViewer;
    @BeforeEach
    void setUp(){
        entity = new Entity(5,5,10,10,"blue");
        gui = Mockito.mock(GUI.class);
        spaceshipViewer = new SpaceshipViewer();
    }
    @Test
    void draw(){
        spaceshipViewer.draw(gui,entity);
        int x,y,width,height;
        x = entity.getX();
        y = entity.getY();
        width = entity.getWidth();
        height = entity.getHeight();
        String color = entity.getColor();
        Mockito.verify(gui,Mockito.times(1)).drawSquare(color,width/2 + x,y - 1);
        for(int i = 0; i < height; i++) {
            Mockito.verify(gui,Mockito.times(1)).drawLine(color,x,y + i, width);
        }
    }

}
