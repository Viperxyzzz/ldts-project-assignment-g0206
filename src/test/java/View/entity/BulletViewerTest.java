package View.entity;

import Model.entity.Entity;
import View.API.GUI;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class BulletViewerTest {
    private GUI gui;
    private Entity entity;
    private BulletViewer bulletViewer;

    @BeforeEach
    void setUp(){
        bulletViewer = new BulletViewer();
        entity = new Entity(5,5,10,14,"blue");
        gui = Mockito.mock(GUI.class);
    }

    @Test
    void draw(){
        bulletViewer.draw(gui,entity);
        int height = entity.getHeight();
        int x = entity.getX();
        int y = entity.getY();
        String color = entity.getColor();
        for(int i = 0; i < height; i++){
            Mockito.verify(gui,Mockito.times(1)).drawSquare(color,x,y + i);
        }
    }
}
