package View.entity;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EntityViewerFactoryTest {
    private String spaceshipType;
    private String alienType;
    private String bulletType;
    private String aliengroupType;
    private EntityViewerFactory entityViewerFactory;

    @BeforeEach
    void setUp(){
        entityViewerFactory = new EntityViewerFactory();
        spaceshipType = "spaceship";
        alienType = "alien";
        bulletType ="bullet";
        aliengroupType = "aliengroup";
    }
    @Test
    void getEntityViewerSpaceship(){
        Assertions.assertTrue(entityViewerFactory.getEntityViewer(spaceshipType) instanceof SpaceshipViewer);
    }

    @Test
    void getEntityViewerAlien(){
        Assertions.assertTrue(entityViewerFactory.getEntityViewer(alienType) instanceof AlienViewer);
    }

    @Test
    void getEntityViewerBullet(){
        Assertions.assertTrue(entityViewerFactory.getEntityViewer(bulletType) instanceof BulletViewer);
    }

    @Test
    void getEntityViewerAlienGroup(){
        Assertions.assertTrue(entityViewerFactory.getEntityViewer(aliengroupType) instanceof AlienGroupViewer);
    }
}
