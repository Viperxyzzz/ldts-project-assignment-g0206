package View;

import Controller.Game;
import Model.states.MenuModel;
import View.API.GUI;
import View.button.ButtonViewer;
import View.states.MenuViewer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;

public class MenuViewerTest {
    private GUI gui;
    private MenuModel menuModel;
    private MenuViewer menuViewer;
    @BeforeEach
    void setUp(){
        gui = Mockito.mock(GUI.class);
        Game game = Mockito.mock(Game.class);
        this.menuModel = new MenuModel(game,this.gui);
        this.menuViewer = new MenuViewer(this.menuModel,this.gui);
    }
    @Test
    void draw() throws IOException {
        ButtonViewer buttonViewer = Mockito.mock(ButtonViewer.class);
        menuViewer.setButtonViewer(buttonViewer);
        menuViewer.draw();
        for(int i = 0; i < menuModel.getButtonList().size();i++){
            Mockito.verify(buttonViewer,Mockito.times(1)).draw(this.gui,menuModel.getButtonList().get(i));
        }
    }
}
