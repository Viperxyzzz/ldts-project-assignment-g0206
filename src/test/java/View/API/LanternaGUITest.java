package View.API;

import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.screen.TerminalScreen;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;

import java.io.IOException;

public class LanternaGUITest {
    private TerminalScreen screen;
    private TextGraphics graphics;
    private LanternaGUI lanternaGUI;

    @BeforeEach
    void setUp() throws IOException {
        screen = Mockito.mock(TerminalScreen.class);
        graphics = Mockito.mock(TextGraphics.class);

        Mockito.when(screen.newTextGraphics()).thenReturn(graphics);

        lanternaGUI = new LanternaGUI(screen);
    }

    @Test
    public void drawText(){
        int x = 0;
        int y = 0;
        String text = "cute";
        lanternaGUI.drawText("blue",text,x,y);

        Mockito.verify(graphics,Mockito.times(1)).setForegroundColor(TextColor.Factory.fromString("white"));
        Mockito.verify(graphics,Mockito.times(1)).setBackgroundColor(TextColor.Factory.fromString("black"));
        Mockito.verify(graphics,Mockito.times(1)).putString(x,y,text);

    }
    @Test
    public void clearScreen(){
        lanternaGUI.clearScreen();
        Mockito.verify(screen,Mockito.times(1)).clear();
    }

    @Test
    public void getKey(){
        System.out.print("not implemented");
    }

    @Test
    public void refresh() throws IOException {
        lanternaGUI.refresh();
        Mockito.verify(screen,Mockito.times(1)).refresh();
    }

    @Test
    public void drawSquare(){
        String color = "blue";
        int x = 0;
        int y = 0;
        lanternaGUI.drawSquare(color,x,y);
        Mockito.verify(graphics,Mockito.times(1)).setBackgroundColor(TextColor.Factory.fromString(color));
        Mockito.verify(graphics,Mockito.times(1)).setCharacter(x,y,' ');
    }

    @Test
    public void drawLine(){
        String color = "blue";
        int x = 0;
        int y = 0;
        int width = 10;
        lanternaGUI.drawLine(color,x,y,width);
        Mockito.verify(graphics,Mockito.times(1)).setBackgroundColor(TextColor.Factory.fromString(color));
        Mockito.verify(graphics,Mockito.times(1)).drawLine(x,y,x+width,y,' ');
    }

    @Test
    public void drawRectangle(){
        String color = "blue";
        int x = 0;
        int y = 0;
        int width = 10;
        int height = 10;
        lanternaGUI.drawRectangle(color,x,y,width,height);

        Mockito.verify(graphics,Mockito.times(1)).setBackgroundColor(TextColor.Factory.fromString(color));
        for(int i = 0; i < height; i++){
            Mockito.verify(graphics,Mockito.times(height)).drawLine(x,y,x+width,y,' ');
        }
    }

}
