package Model.entity;

import java.util.ArrayList;
import java.util.List;

public class AlienGroup extends Alien{
    private List<Alien> alienGroup;
    public AlienGroup(int x, int y, int width, int height, String color,int numberOfAliens) {
        super(x, y, width, height, color);
        this.alienGroup = new ArrayList<Alien>();
        this.type = "aliengroup";
        int counter = 0;
        for(int i = 0; i < numberOfAliens; i++){
            if(x + counter >= 70){
                y += height;
                counter = 0;
            }
            this.alienGroup.add(new Alien(x + counter,y,width,height,color));
            counter += 2 * width;
        }
    }

    public List<Alien> getAlienGroup(){
        return this.alienGroup;
    }

    public int getTotalAliens(){
        return this.alienGroup.size();
    }

}
