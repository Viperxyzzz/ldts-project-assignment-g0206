package Model.entity;

public class Bullet extends MovableEntity{
    private int damage;
    private boolean isMoving;
    public Bullet(int x, int y, int width, int height, String color, int speed,int damage) {
        super(x, y, width, height, color, speed);
        this.type = "bullet";
        this.damage = damage;
        this.isMoving = false;
    }

    public void setIsMoving(boolean value){
        this.isMoving = value;
    }

    public boolean getIsMoving(){
        return this.isMoving;
    }
    public int getDamage(){
        return this.damage;
    }
}
