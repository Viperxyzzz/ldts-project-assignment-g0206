package Model.entity;

public class Alien extends Entity {
    public Alien(int x, int y, int width, int height, String color) {
        super(x, y, width, height, color);
        this.type = "alien";
    }
}
