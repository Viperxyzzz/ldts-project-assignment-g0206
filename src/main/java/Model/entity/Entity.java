package Model.entity;

public class Entity {
    private int x,y,width,height;
    private String color;
    protected String type; //this one will be used for factory
    public Entity(int x, int y,int width,int height, String color){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.color = color;
        this.type = "entity";
    }
    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }
    public int getX() { return this.x;}
    public int getY() { return this.y;}
    public int getWidth() { return this.width;}
    public int getHeight() {return this.height;}
    public String getColor() {return this.color;}
    public String getType() {return this.type;}
    @Override
    public boolean equals(Object o){
        if(o == this){
            return true;
        }
        if(!(o instanceof Entity)){
            return false;
        }
        Entity object = (Entity) o;
        return object.getType() == this.getType() &&
                object.getX() == this.getX() &&
                object.getY() == this.getY() &&
                object.getWidth() == this.getWidth() &&
                object.getHeight() == this.getHeight();
    }
}
