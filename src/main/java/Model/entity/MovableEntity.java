package Model.entity;

public class MovableEntity extends Entity {
    private int speed;
    public MovableEntity(int x, int y,int width, int height, String color, int speed) {
        super(x, y,width,height, color);
        this.speed = speed;
        this.type = "movableentity";
    }
    public int getSpeed(){
        return this.speed;
    }
    public void setSpeed(int speed){
        this.speed = speed;
    }
}
