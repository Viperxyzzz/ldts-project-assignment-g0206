package Model.entity;

public class Spaceship extends MovableEntity{
    private int bulletDamage;
    private int totalBullets;
    public Spaceship(int x, int y,int width,int height, String color, int speed) {
        super(x, y,width,height, color, speed);
        this.totalBullets = 30;
        this.type = "spaceship";
    }
    public void setTotalBullets(int ammo){
        this.totalBullets = ammo;
    }
    public int getTotalBullets() {return this.totalBullets;}
}

