package Model.uibar;

import View.API.GUI;

public class UIBar {
    private int totalBullets;
    private int height;
    private GUI gui;
    public UIBar(int totalBullets, int height){
        this.totalBullets = totalBullets;
        this.height = height;

    }

    public int getHeight(){
        return this.height;
    }

    public int getTotalBullets(){
        return this.totalBullets;
    }

    public void setTotalBullets(int amount){
        this.totalBullets = amount;
    }
}
