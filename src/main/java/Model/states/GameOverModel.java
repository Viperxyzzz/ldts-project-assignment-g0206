package Model.states;

public class GameOverModel {
    private String message;
    private String blinkingMessage;
    private boolean visible;

    public GameOverModel(boolean won) {
        this.message = won ? "WIN" : "LOSE";
        this.blinkingMessage = "Press ENTER key to continue";
        this.visible = true;
    }

    public boolean getVisible(){
        return this.visible;
    }

    public void setVisible(boolean visible){
        this.visible = visible;
    }

    public String getMessage(){
        return this.message;
    }

    public String getBlinkingMessage(){
        return this.blinkingMessage;
    }

}
