package Model.states;

import Controller.buttons.ButtonCommand;
import Controller.buttons.ExitCommand;
import Controller.buttons.PlayCommand;
import Controller.Game;
import Model.buttons.Button;
import View.API.GUI;

import java.util.ArrayList;
import java.util.List;

public class MenuModel {
    private List<Button> buttonList;
    int buttonIndex = 0;
    public MenuModel(Game game, GUI gui){
        this.buttonList = new ArrayList<Button>();


        //Buttons
        ButtonCommand playCommand = new PlayCommand(game,gui);
        Button playButton = new Button(playCommand,20,20,30,10,"Play");
        playButton.setHover(true);
        buttonList.add(playButton);
        ButtonCommand exitCommand = new ExitCommand(game,gui);
        Button exitButton = new Button(exitCommand,20,40,30,10,"Exit");
        buttonList.add(exitButton);

    }


    public List<Button> getButtonList() {
        return buttonList;
    }


    public void nextButton(){
        buttonIndex++;
        if(buttonIndex >= buttonList.size())
        {
            buttonIndex = 0;
        }
    }

    public void previousButton(){
        buttonIndex--;
        if(buttonIndex < 0){
            buttonIndex = buttonList.size() - 1;
        }
    }

    public Button getCurrentButton(){
        return this.buttonList.get(buttonIndex);
    }


}
