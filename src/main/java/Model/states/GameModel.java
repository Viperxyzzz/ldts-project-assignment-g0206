package Model.states;

import Model.entity.AlienGroup;
import Model.entity.Entity;
import Model.entity.Spaceship;

import java.util.ArrayList;
import java.util.List;

public class GameModel {
    private List<Entity> entityList;
    private AlienGroup alienGroup;
    private boolean running;
    public GameModel(){
        this.entityList = new ArrayList<Entity>();
        this.running = true;


        Entity spaceship = new Spaceship(10, 60 * 4/5, 20, 3, "green", 2 );
        this.alienGroup = new AlienGroup(10, 60 * 1/4,4,3,"blue",32);
        entityList.add(spaceship);
        entityList.add(alienGroup);

    }

    public Entity getSpaceship(){
        for(Entity entity : entityList){
            if(entity.getType() == "spaceship"){
                return entity;
            }
        }
        return null;
    }

    public AlienGroup getAlienGroup() {
        return alienGroup;
    }


    public List<Entity> getEntityList(){
        return this.entityList;
    }

    public boolean isRunning() {
        return this.running;
    }

    public void setRunning(boolean b) {
        this.running = b;
    }
}
