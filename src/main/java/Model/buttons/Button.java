package Model.buttons;

import Controller.buttons.ButtonCommand;

public class Button {
    private ButtonCommand buttonCommand;
    private int width;
    private int height;
    private int x;
    private int y;
    private boolean hover;
    private String text;

    public Button(ButtonCommand buttonCommand,int x, int y, int width, int height, String text){
        this.buttonCommand = buttonCommand;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.text = text;
        this.hover = false;
    }

    public ButtonCommand getButtonCommand(){
        return this.buttonCommand;
    }

    public void setHover(boolean value){
        this.hover = value;
    }

    public String getText(){
        return this.text;
    }

    public int getWidth(){
        return this.width;
    }

    public int getHeight(){
        return this.height;
    }

    public int getX(){
        return this.x;
    }

    public int getY(){
        return this.y;
    }

    public boolean isHovering(){
        return this.hover;
    }

}
