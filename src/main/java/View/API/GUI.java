package View.API;


import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;

import java.io.IOException;

public interface GUI {
    void drawText(String color,String text,int x, int y);
    void clearScreen();
    KeyType getKey() throws IOException;
    KeyType getKeyPolling() throws IOException;
    void refresh() throws IOException;
    void drawSquare(String color,int x, int y);
    void drawLine(String color,int x, int y,int width);
    void drawRectangle(String color, int x, int y,int width,int height);
    int getHeight();
    int getWidth();
    void close() throws IOException;
}
