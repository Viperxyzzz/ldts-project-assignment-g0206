package View.API;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;

import java.io.IOException;


public class LanternaGUI implements GUI{
    private int width;
    private int height;
    private TerminalScreen screen;
    private TextGraphics graphic;
    public LanternaGUI(int width, int height) throws IOException {
        this.width = width;
        this.height = height;
        DefaultTerminalFactory factory = new DefaultTerminalFactory();
        factory.setInitialTerminalSize(new TerminalSize(width, height));
        Terminal terminal = factory.createTerminal();
        this.screen = new TerminalScreen(terminal);
        this.screen.setCursorPosition(null); // we don't need a cursor
        this.screen.startScreen(); // screens must be started
        this.screen.doResizeIfNecessary(); // resize screen if
        this.screen.clear();
        this.graphic = screen.newTextGraphics();
    }
    public LanternaGUI(TerminalScreen screen){
        this.screen = screen;
        this.graphic = this.screen.newTextGraphics();
    }
    public int getWidth(){
        return this.width;
    }
    public int getHeight(){
        return this.height;
    }
    @Override
    public void drawText(String color,String text, int x, int y) {
        graphic.setForegroundColor(TextColor.Factory.fromString("white"));
        graphic.setBackgroundColor(TextColor.Factory.fromString("black"));
        graphic.putString(x,y,text);
    }

    @Override
    public void clearScreen() {
        this.screen.clear();
    }

    @Override
    public KeyType getKey() throws IOException {
        KeyStroke key = screen.readInput();
        return key.getKeyType();
    }

    @Override
    public KeyType getKeyPolling() throws IOException {
        KeyStroke key = screen.pollInput();
        if(key == null)
            return KeyType.Unknown;
        return key.getKeyType();
    }

    @Override
    public void refresh() throws IOException {
        this.screen.refresh();
    }
    @Override
    public void drawSquare(String color, int x, int y){
        graphic.setBackgroundColor(TextColor.Factory.fromString(color));
        graphic.setCharacter(x,y,' ');
    }
    @Override
    public void drawLine(String color, int x, int y, int width) {
            graphic.setBackgroundColor(TextColor.Factory.fromString(color));
            graphic.drawLine(x,y,x + width,y,' ');
    }
    @Override
    public void drawRectangle(String color, int x, int y, int width, int height){
        graphic.setBackgroundColor(TextColor.Factory.fromString(color));
        for(int i = 0; i < height; i++){
            graphic.drawLine(x,y,x+width,y,' ');
        }
    }
    @Override
    public void close() throws IOException {
        this.screen.close();
    }

}
