package View.uibar;

import Model.uibar.UIBar;
import View.API.GUI;

public class UIBarViewer {
    private GUI gui;
    private UIBar uiBar;
    public UIBarViewer(GUI gui,UIBar uiBar){
        this.gui = gui;
        this.uiBar = uiBar;
    }
    public void draw(){
        for(int i = 0; i < 8; i++) {
            this.gui.drawLine("#964B00", 0, this.uiBar.getHeight() - i, 80);
        }
        this.gui.drawText("white","Bullets " + Integer.toString(this.uiBar.getTotalBullets()),1,this.uiBar.getHeight() -5);
    }
}
