package View.button;

import Model.buttons.Button;
import View.API.GUI;

public class ButtonViewer {
    public void draw(GUI gui, Button button){
        int width = button.getWidth();
        int height = button.getHeight();
        int x = button.getX();
        int y = button.getY();

        for(int i = 0; i < height; i++){
            gui.drawLine("red",x,y+i,width);
            if(button.isHovering()){
                if(i == 0 || i == height - 1){
                    gui.drawLine("#FFFF00",x,y + i,width);
                }
                else{
                    gui.drawSquare("#FFFF00",x,y+i);
                    gui.drawSquare("#FFFF00",x + width,y+i);
                }
            }
            if(i == height / 2){
                gui.drawText("white",button.getText(),(width/2 + x),y + i);
            }
        }

    }
}
