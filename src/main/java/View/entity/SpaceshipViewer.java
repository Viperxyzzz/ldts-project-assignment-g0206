package View.entity;

import Model.entity.Entity;
import View.API.GUI;

public class SpaceshipViewer implements EntityViewer{
    @Override
    public void draw(GUI gui, Entity entity) {
        int x,y,width,height;
        x = entity.getX();
        y = entity.getY();
        width = entity.getWidth();
        height = entity.getHeight();
        String color = entity.getColor();
        gui.drawSquare(color,width/2 + x,y - 1);
        for(int i = 0; i < height; i++) {
            gui.drawLine(color,x,y + i, width);
        }

    }
}
