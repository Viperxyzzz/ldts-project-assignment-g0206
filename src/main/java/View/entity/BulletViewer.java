package View.entity;

import Model.entity.Entity;
import View.API.GUI;

public class BulletViewer implements EntityViewer{
    @Override
    public void draw(GUI gui, Entity entity) {
        int height = entity.getHeight();
        int x = entity.getX();
        int y = entity.getY();
        String color = entity.getColor();
        for(int i = 0; i < height; i++){
            gui.drawSquare(color,x,y + i);
        }
    }
}
