package View.entity;

import Model.entity.Entity;
import View.API.GUI;

public class AlienViewer implements EntityViewer{
    @Override
    public void draw(GUI gui, Entity entity) {
        int height = entity.getHeight();
        int x = entity.getX();
        int y = entity.getY();
        int width = entity.getWidth();
        String color = entity.getColor();
        for(int i = 0; i < height; i++){
            gui.drawLine(color, x, y, width);
        }
        gui.drawSquare(color, x + width - 1, y + 1);
        gui.drawSquare(color, x + 1, y + 1);
    }
}
