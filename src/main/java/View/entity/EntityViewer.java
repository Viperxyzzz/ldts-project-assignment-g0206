package View.entity;

import Model.entity.Entity;
import View.API.GUI;

public interface EntityViewer {
    public void draw(GUI gui, Entity entity);
}