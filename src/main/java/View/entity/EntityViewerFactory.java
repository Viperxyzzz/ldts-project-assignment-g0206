package View.entity;

public class EntityViewerFactory {
    public EntityViewer getEntityViewer(String viewType){
        switch(viewType){
            case "spaceship":
                return new SpaceshipViewer();
            case "alien":
                return new AlienViewer();
            case "bullet":
                return new BulletViewer();
            case "aliengroup":
                return new AlienGroupViewer();
            default:
                return null;
        }
    }
}
