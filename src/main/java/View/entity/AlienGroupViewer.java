package View.entity;

import Model.entity.Alien;
import Model.entity.AlienGroup;
import Model.entity.Entity;
import View.API.GUI;

import java.util.List;

public class AlienGroupViewer implements EntityViewer{
    private AlienViewer alienViewer;
    public AlienGroupViewer(){
        this.alienViewer = new AlienViewer();
    }
    @Override
    public void draw(GUI gui, Entity entity) {
        List<Alien> alienList = ((AlienGroup) entity).getAlienGroup();
        for(Alien alien : alienList){
            alienViewer.draw(gui,alien);
        }
    }
}
