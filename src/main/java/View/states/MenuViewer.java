package View.states;

import Model.states.MenuModel;
import Model.buttons.Button;
import View.API.GUI;
import View.button.ButtonViewer;

import java.io.IOException;
import java.util.List;

public class MenuViewer {
    private GUI gui;
    private MenuModel menuModel;
    private ButtonViewer buttonViewer;

    public MenuViewer(MenuModel menuModel, GUI gui){

        this.menuModel = menuModel;
        this.gui = gui;
        this.buttonViewer = new ButtonViewer();
    }

    public void draw() throws IOException {

        List<Button> buttonList = this.menuModel.getButtonList();

        for(Button button : buttonList){
            this.buttonViewer.draw(this.gui,button);
        }
    }
    public void setButtonViewer(ButtonViewer buttonViewer){
        this.buttonViewer = buttonViewer;
    }

}