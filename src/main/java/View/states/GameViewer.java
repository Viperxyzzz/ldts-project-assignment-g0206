package View.states;

import Model.entity.Entity;
import View.API.GUI;
import View.entity.EntityViewer;
import View.entity.EntityViewerFactory;

import java.util.List;

public class GameViewer {
    private GUI gui;
    public GameViewer(GUI gui){
        this.gui = gui;
    }

    public void draw(List<Entity> entityList){
        EntityViewerFactory entityViewerFactory = new EntityViewerFactory();
        for(Entity entity : entityList){
            EntityViewer entityViewer = entityViewerFactory.getEntityViewer(entity.getType());
            entityViewer.draw(this.gui,entity);
        }
    }
}
