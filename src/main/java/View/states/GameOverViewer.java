package View.states;

import Model.states.GameOverModel;
import View.API.GUI;

public class GameOverViewer {
    private GUI gui;
    private GameOverModel gameOverModel;
    public GameOverViewer(GUI gui, GameOverModel gameOverModel){
        this.gui = gui;
        this.gameOverModel = gameOverModel;
    }

    public void draw(){
        this.gui.drawText("blue", gameOverModel.getMessage(), 35,10);

        if(gameOverModel.getVisible()){

            this.gui.drawText("white",gameOverModel.getBlinkingMessage(),25,30);
        }
    }

}
