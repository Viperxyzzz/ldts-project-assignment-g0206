package Controller.observer;

import Controller.entity.SpaceshipController;
import Controller.bullet.CantShootStrategy;
import Controller.bullet.NormalBulletStrategy;

public class BulletStrategyObserver implements GameEventObserver{
    private SpaceshipController spaceshipController;

    public BulletStrategyObserver(SpaceshipController spaceshipController){
        this.spaceshipController = spaceshipController;
    }
    @Override
    public void update(int data) {
        if(data <= 0){
            spaceshipController.setBulletStrategy(new CantShootStrategy());
        }
        else{
            spaceshipController.setBulletStrategy(new NormalBulletStrategy());
        }
    }
}
