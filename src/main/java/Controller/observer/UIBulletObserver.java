package Controller.observer;

import Model.uibar.UIBar;

public class UIBulletObserver implements GameEventObserver{
    private UIBar uiBar;
    public UIBulletObserver(UIBar uiBar){
        this.uiBar = uiBar;
    }

    @Override
    public void update(int data) {
        uiBar.setTotalBullets(data);
    }
}
