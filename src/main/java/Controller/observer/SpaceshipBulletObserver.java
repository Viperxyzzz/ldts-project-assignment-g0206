package Controller.observer;

import Model.entity.Spaceship;

public class SpaceshipBulletObserver implements GameEventObserver{
    private Spaceship spaceship;

    public SpaceshipBulletObserver(Spaceship spaceship){
        this.spaceship = spaceship;
    }

    @Override
    public void update(int data) {
        spaceship.setTotalBullets(data);
    }
}
