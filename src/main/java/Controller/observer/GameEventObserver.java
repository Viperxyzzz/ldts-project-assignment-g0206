package Controller.observer;

public interface GameEventObserver {
    void update(int data);
}
