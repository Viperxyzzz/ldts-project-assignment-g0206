package Controller.observer;

public interface Observer {
    void update();
}
