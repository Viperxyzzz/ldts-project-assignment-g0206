package Controller.observer;

public class KeyObserver implements Observer{
    private boolean keyPressed = false;
    @Override
    public void update(){
        keyPressed = true;
    }
    public boolean getKeyPressed(){
        return keyPressed;
    }
}
