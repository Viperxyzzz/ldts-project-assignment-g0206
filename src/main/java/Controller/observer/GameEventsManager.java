package Controller.observer;

import java.util.HashMap;
import java.util.Map;

public class GameEventsManager {
    private Map<String, GameEventObserver> gameEventsListeners = new HashMap<String,GameEventObserver>();
    public void subscribe(String eventType, GameEventObserver observer){
        gameEventsListeners.put(eventType,observer);
    }
    public void unsubscribe(String eventType, GameEventObserver observer){
        gameEventsListeners.remove(eventType,observer);
    }
    public void notify(String eventType,int data){
        gameEventsListeners.get(eventType).update(data);
    }

}
