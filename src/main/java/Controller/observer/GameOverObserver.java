package Controller.observer;

import Model.states.GameModel;

public class GameOverObserver implements GameEventObserver{
    private GameModel gameModel;
    public GameOverObserver(GameModel gameModel){
        this.gameModel = gameModel;
    }
    @Override
    public void update(int data) {
        if(data == 0) {
            gameModel.setRunning(false);
        }
    }
}
