package Controller.observer;

import java.util.ArrayList;
import java.util.List;

public class KeySubject {
    private List<Observer> keyObservers = new ArrayList<Observer>();
    public void addObserver(Observer observer){
        this.keyObservers.add(observer);
    }
    public void removeObserver(Observer observer){
        this.keyObservers.remove(observer);
    }
    public void notifyObservers(){
        for(Observer observer : keyObservers){
            observer.update();
        }
    }
}
