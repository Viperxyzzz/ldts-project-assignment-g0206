package Controller.move;

import Model.entity.MovableEntity;

public class MoveLeftCommand implements Move{
    private MovableEntity entity;
    public MoveLeftCommand(MovableEntity entity){
        this.entity = entity;
    }
    @Override
    public void execute() {
        this.entity.setPosition(this.entity.getX() - (1*this.entity.getSpeed()),this.entity.getY());
    }
}
