package Controller.move;

import java.util.List;



public class MoveInvoker {
    private List<Move> moveList;
    public MoveInvoker(List<Move> moveList){
        this.moveList = moveList;
    }
    public void applyMoves(){
        for(Move move : moveList){
            move.execute();
        }
    }
}
