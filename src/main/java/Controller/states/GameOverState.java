package Controller.states;

import Controller.Game;
import Controller.observer.KeyObserver;
import Controller.observer.KeySubject;
import Model.states.GameOverModel;
import View.API.GUI;
import View.states.GameOverViewer;

import java.io.IOException;

public class GameOverState extends State{
    private GUI gui;
    private GameOverModel gameOverModel;
    private GameOverViewer gameOverViewer;
    private KeySubject keySubject;
    private KeyObserver keyObserver;


    public GameOverState(Game game, GUI gui,boolean won){
        this.gui = gui;
        this.game = game;

        this.gameOverModel = new GameOverModel(won);
        this.gameOverViewer = new GameOverViewer(gui,this.gameOverModel);
        this.keySubject = new KeySubject();
        this.keyObserver = new KeyObserver();
        this.keySubject.addObserver(this.keyObserver);
    }

    public void setKeyObserver(KeyObserver  keyObserver2){
        this.keySubject.removeObserver(this.keyObserver);
        this.keyObserver = keyObserver2;
        this.keySubject.addObserver(keyObserver2);
    }

    public void handleKeyPress() throws IOException {
        switch(this.gui.getKeyPolling()){
            case Enter:
                this.keySubject.notifyObservers();
            default:
                this.gameOverModel.setVisible(!this.gameOverModel.getVisible());
                break;
        }
    }

    @Override
    protected void setState() {
        this.game.setState(new MenuState(this.game,this.gui));
    }

    @Override
    public void run() throws IOException, InterruptedException {
        while(true){
            this.gui.clearScreen();
            this.gameOverViewer.draw();
            this.gui.refresh();
            this.handleKeyPress();
            if(keyObserver.getKeyPressed()){
                this.setState();
                return;
            }
            Thread.sleep(400);
        }

    }
}
