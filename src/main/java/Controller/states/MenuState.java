package Controller.states;

import Controller.Game;
import Controller.observer.KeyObserver;
import Controller.observer.KeySubject;
import Model.states.MenuModel;
import View.API.GUI;
import View.states.MenuViewer;

import java.io.IOException;

public class MenuState extends State{
    private GUI gui;
    private MenuModel menuModel;
    private KeySubject keySubject;
    private KeyObserver keyObserver;
    private MenuViewer menuViewer;
    public MenuState(Game game, GUI gui){
        this.game = game;
        this.gui = gui;

        this.menuModel = new MenuModel(this.game,this.gui);
        this.menuViewer = new MenuViewer(this.menuModel,this.gui);
        //Observers
        this.keyObserver = new KeyObserver();
        this.keySubject = new KeySubject();
        this.keySubject.addObserver(keyObserver);
    }
  
    public void setKeyObserver(KeyObserver keyObserver2){
        this.keySubject.removeObserver(this.keyObserver);
        this.keyObserver = keyObserver2;
        this.keySubject.addObserver(keyObserver2);
    }

    public void handleKeyPress() throws IOException {
        switch(this.gui.getKey()){
            case ArrowDown:
                this.menuModel.getCurrentButton().setHover(false);
                this.menuModel.nextButton();
                this.menuModel.getCurrentButton().setHover(true);
                break;
            case ArrowUp:
                this.menuModel.getCurrentButton().setHover(false);
                this.menuModel.previousButton();
                this.menuModel.getCurrentButton().setHover(true);
                break;
            case Enter:
                this.menuModel.getCurrentButton().getButtonCommand().execute();
                this.keySubject.notifyObservers();
                break;
            default:
                break;

        }
    }


    @Override
    public void run() throws IOException {
        while(true) {
            this.gui.clearScreen();
            this.menuViewer.draw();
            this.gui.refresh();
            this.handleKeyPress();
            if(keyObserver.getKeyPressed())
                return;
        }
    }

    @Override
    protected void setState() {
    }


}
