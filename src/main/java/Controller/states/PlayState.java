package Controller.states;

import Controller.entity.CollisionController;
import Controller.entity.SpaceshipController;
import Controller.Game;
import Controller.bullet.BulletLogic;
import Controller.bullet.NormalBulletStrategy;
import Controller.move.Move;
import Controller.move.MoveInvoker;
import Controller.move.MoveLeftCommand;
import Controller.move.MoveRightCommand;
import Controller.observer.*;
import Model.states.GameModel;
import Model.uibar.UIBar;
import Model.entity.*;
import View.API.GUI;
import View.states.GameViewer;
import View.uibar.UIBarViewer;



import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class PlayState extends State{
    private BulletLogic bulletLogic;
    private GUI gui;
    private GameViewer gameViewer;
    private GameModel gameModel;
    private GameEventsManager gameEventsManager;
    private Spaceship spaceship;
    private SpaceshipController spaceshipController;
    private UIBar uiBar;
    private UIBarViewer uiBarViewer;
    private CollisionController collisionController;
    private boolean won;
    public PlayState(Game game, GUI gui){
        this.game = game;
        this.gui = gui;
        initGame();
        initGameObservers();
    }

    private void initGameObservers(){
        UIBulletObserver uiBulletObserver = new UIBulletObserver(this.uiBar);
        SpaceshipBulletObserver spaceshipBulletObserver = new SpaceshipBulletObserver(spaceship);
        BulletStrategyObserver bulletStrategyObserver = new BulletStrategyObserver(this.spaceshipController);
        GameOverObserver gameOverObserver = new GameOverObserver(this.gameModel);
        this.gameEventsManager = new GameEventsManager();
        this.gameEventsManager.subscribe("gameOver",gameOverObserver);
        this.gameEventsManager.subscribe("bulletStrategy",bulletStrategyObserver);
        this.gameEventsManager.subscribe("bulletUI",uiBulletObserver);
        this.gameEventsManager.subscribe("bulletSpaceship",spaceshipBulletObserver);
    }

    private void initGame(){
        this.gameViewer = new GameViewer(this.gui);
        this.gameModel = new GameModel();
        this.bulletLogic = new BulletLogic();
        this.uiBar = new UIBar(((Spaceship) gameModel.getSpaceship()).getTotalBullets(),this.game.getHeight());
        this.uiBarViewer = new UIBarViewer(this.gui,this.uiBar);
        this.spaceship = (Spaceship) gameModel.getSpaceship();
        this.spaceshipController = new SpaceshipController(spaceship,new NormalBulletStrategy());
        this.collisionController = new CollisionController();
        this.won = false;

    }


    public void handleKeyPress(List<Move> moveList, List<Entity> entityList) throws IOException {
        switch (this.gui.getKeyPolling()) {
            case ArrowLeft:
                spaceship.setPosition(spaceship.getX() - 1, spaceship.getY());
                if(collisionController.canMove(spaceship)) moveList.add(new MoveLeftCommand((MovableEntity) spaceship));
                spaceship.setPosition(spaceship.getX() + 1, spaceship.getY());
                break;
            case ArrowRight:
                spaceship.setPosition(spaceship.getX() + 1, spaceship.getY());
                if(collisionController.canMove(spaceship)) moveList.add(new MoveRightCommand((MovableEntity) spaceship));
                spaceship.setPosition(spaceship.getX() - 1, spaceship.getY());
                break;
            case ArrowUp:
                Bullet bullet = spaceshipController.shoot();
                if(bullet != null) {
                    entityList.add(bullet);
                    bulletLogic.addBullet(bullet);
                }
                gameEventsManager.notify("bulletSpaceship",((Spaceship) spaceship).getTotalBullets());
                gameEventsManager.notify("bulletStrategy",((Spaceship)spaceship).getTotalBullets());
                gameEventsManager.notify("bulletUI",((Spaceship) spaceship).getTotalBullets());
                gameEventsManager.notify("gameOver",((Spaceship)spaceship).getTotalBullets() + bulletLogic.getBulletList().size());
                break;
            case Escape:
                gameEventsManager.notify("gameOver",0);
            default:
                break;
        }
    }

    private void handleMovement(List<Entity> entityList) throws IOException {
        List<Move> moveList = new ArrayList<Move>();
        handleKeyPress(moveList,entityList);
        bulletLogic.updateBullets();
        MoveInvoker moveInvoker = new MoveInvoker(moveList);
        moveInvoker.applyMoves();
    }

    private void handleCollisions(List<Alien> alienList){
        List<Bullet> bulletList = bulletLogic.getBulletList();
        collisionController.setAlienList(alienList);
        collisionController.setBulletList(bulletList);
        collisionController.handleCollisions(gameEventsManager,spaceship);
        collisionController.checkBoundaries();
    }

    private void updateAfterCollisions(List<Entity> entityList, List<Alien> alienList){
        entityList.removeAll(collisionController.getDestroyedBullets());
        bulletLogic.getBulletList().removeAll(collisionController.getDestroyedBullets());
        alienList.removeAll(collisionController.getDestroyedAliens());
        gameEventsManager.notify("gameOver",alienList.size());
        collisionController.reset();
    }

    private void updateViewers(List<Entity> entityList) throws IOException, InterruptedException {
        Thread.sleep(24);
        this.gameViewer.draw(entityList);
        this.uiBarViewer.draw();
        this.gui.refresh();
    }

    @Override
    protected void setState() {
        this.game.setState(new GameOverState(this.game,this.gui,this.won));
    }


    @Override
    public void run() throws IOException, InterruptedException {
        List<Alien> alienList = new ArrayList<Alien>();
        while(gameModel.isRunning()) {
            this.gui.clearScreen();
            List<Entity> entityList = gameModel.getEntityList();
            alienList = this.gameModel.getAlienGroup().getAlienGroup();
            this.handleMovement(entityList);
            this.handleCollisions(alienList);
            gameEventsManager.notify("gameOver",((Spaceship)spaceship).getTotalBullets() + bulletLogic.getBulletList().size());
            this.updateAfterCollisions(entityList,alienList);
            this.updateViewers(entityList);
        }
        this.won = alienList.size() == 0;
        this.setState();
    }

}
