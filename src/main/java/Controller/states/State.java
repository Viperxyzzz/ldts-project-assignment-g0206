package Controller.states;

import Controller.Game;
import View.API.GUI;

import java.io.IOException;

public abstract class State {
    protected Game game;
    public abstract void run() throws IOException, InterruptedException;
    protected abstract void setState();
}
