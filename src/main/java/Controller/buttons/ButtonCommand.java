package Controller.buttons;

import java.io.IOException;

public interface ButtonCommand {
    public void execute() throws IOException;
}
