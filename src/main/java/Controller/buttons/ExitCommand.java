package Controller.buttons;

import Controller.Game;
import View.API.GUI;

import java.io.IOException;

public class ExitCommand implements ButtonCommand {
    private Game game;
    private GUI gui;
    public ExitCommand(Game game,GUI gui) {
        this.game = game;
        this.gui = gui;
    }
    @Override
    public void execute() throws IOException {
        this.gui.close();
        this.game.stopRunning();
    }
}
