package Controller.buttons;

import Controller.Game;
import Controller.states.PlayState;
import View.API.GUI;

public class PlayCommand implements ButtonCommand{
    private Game game;
    private GUI gui;
    public PlayCommand(Game game, GUI gui){
         this.game = game;
         this.gui = gui;
    }
    @Override
    public void execute() {
        this.game.setState(new PlayState(this.game,this.gui));
    }
}
