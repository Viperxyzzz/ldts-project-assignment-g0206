package Controller.bullet;

import Model.entity.Bullet;
import Model.entity.Spaceship;

public interface BulletStrategy {
    Bullet shoot(Spaceship spaceship);
}
