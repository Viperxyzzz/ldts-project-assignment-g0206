package Controller.bullet;

import Model.entity.Bullet;
import Model.entity.Spaceship;


public class NormalBulletStrategy implements BulletStrategy {
    @Override
    public Bullet shoot(Spaceship spaceship) {
        Bullet bullet = new Bullet(spaceship.getWidth() / 2 + spaceship.getX(), spaceship.getY() - spaceship.getHeight(), 2,3,"blue",1,10);
        bullet.setIsMoving(true);
        spaceship.setTotalBullets(spaceship.getTotalBullets() - 1);
        return bullet;
    }
}
