package Controller.bullet;

import Model.entity.Bullet;

import java.util.ArrayList;
import java.util.List;

public class BulletLogic{
    private List<Bullet> bulletList = new ArrayList<Bullet>();


    public void addBullet(Bullet bullet){
        this.bulletList.add(bullet);
    }

    public List<Bullet> getBulletList() {
        return bulletList;
    }

    public void updateBullets(){
        for(Bullet bullet : bulletList){
            if(bullet.getIsMoving()){
                bullet.setPosition(bullet.getX(),bullet.getY() - bullet.getSpeed());
            }
        }
    }

}
