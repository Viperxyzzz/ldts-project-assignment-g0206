package Controller.bullet;

import Model.entity.Bullet;
import Model.entity.Spaceship;

public class CantShootStrategy implements BulletStrategy {
    @Override
    public Bullet shoot(Spaceship spaceship) {
        return null;
    }
}
