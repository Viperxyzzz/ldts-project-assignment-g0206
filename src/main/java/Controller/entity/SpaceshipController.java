package Controller.entity;

import Controller.bullet.BulletStrategy;
import Model.entity.Bullet;
import Model.entity.Spaceship;

public class SpaceshipController {
    private Spaceship spaceship;
    private BulletStrategy bulletStrategy;

    public SpaceshipController(Spaceship spaceship, BulletStrategy bulletStrategy){
        this.spaceship = spaceship;
        this.bulletStrategy = bulletStrategy;
    }

    public Bullet shoot(){
        return bulletStrategy.shoot(spaceship);
    }

    public void setBulletStrategy(BulletStrategy bulletStrategy){
        this.bulletStrategy = bulletStrategy;
    }

    public BulletStrategy getBulletStrategy(){
        return this.bulletStrategy;
    }
}
