package Controller.entity;

import Controller.observer.GameEventsManager;
import Model.entity.Alien;
import Model.entity.Bullet;
import Model.entity.Entity;
import Model.entity.Spaceship;

import java.util.ArrayList;
import java.util.List;

public class CollisionController {
    private List<Alien> alienList;
    private List<Bullet> bulletList;
    private List<Alien> destroyedAliens;
    private List<Bullet> destroyedBullets;

    public CollisionController(){
        destroyedAliens = new ArrayList<Alien>();
        destroyedBullets = new ArrayList<Bullet>();
    }

    public void setAlienList(List<Alien> alienList) {
        this.alienList = alienList;
    }

    public void setBulletList(List<Bullet> bulletList){
        this.bulletList = bulletList;
    }

    public boolean collision(Entity alien, Entity bullet){
        return ( (alien.getY() == bullet.getY()) && alien.getX() <= bullet.getX() && alien.getX() + alien.getWidth() >= bullet.getX() + bullet.getWidth());
    }

    public void handleCollisions(GameEventsManager gameEventsManager, Spaceship spaceship){
        for(Alien alien : alienList){
            for(Bullet bullet : bulletList) {
                if (this.collision( alien, bullet)) {
                    destroyedAliens.add(alien);
                    destroyedBullets.add(bullet);
                    spaceship.setTotalBullets(spaceship.getTotalBullets() + 1);
                    gameEventsManager.notify("bulletSpaceship",((Spaceship) spaceship).getTotalBullets());
                    gameEventsManager.notify("bulletStrategy",((Spaceship)spaceship).getTotalBullets());
                    gameEventsManager.notify("bulletUI",((Spaceship) spaceship).getTotalBullets());
                }
            }
        }
    }

    public void checkBoundaries(){
        for(Bullet bullet : bulletList){
            if(bullet.getY() <= 0) {
                destroyedBullets.add(bullet);
            }
        }
    }

    public boolean canMove(Entity entity){
        if(entity.getY() <= 0)
            return false;
        if(entity.getX() <= 0)
            return false;
        if(entity.getX() + entity.getWidth() >= 80)
            return false;
        if(entity.getY() + entity.getHeight() >= 60)
            return false;
        return true;
    }

    public void reset(){
        this.destroyedAliens = new ArrayList<Alien>();
        this.destroyedBullets = new ArrayList<Bullet>();
    }

    public List<Alien> getDestroyedAliens() {
        return destroyedAliens;
    }

    public List<Bullet> getDestroyedBullets() {
        return destroyedBullets;
    }

}
