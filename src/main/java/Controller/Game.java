package Controller;

import Controller.states.MenuState;
import Controller.states.State;
import View.API.GUI;
import View.API.LanternaGUI;

import java.io.IOException;

public class Game {
    private GUI gui;
    private State state;
    private int width;
    private int height;
    private boolean running;
    public Game() throws IOException {
        this.width = 80;
        this.height = 60;
        this.gui = new LanternaGUI(width,height);
        this.state = new MenuState(this,gui);
        this.running = true;
    }
    public void setState(State state){
        this.state = state;
    }
    public void run() throws IOException, InterruptedException {
        while(running) {
            this.state.run();
        }
    }
    public void stopRunning(){
        this.running = false;
    }
    public int getWidth(){
        return this.width;
    }
    public int getHeight(){
        return this.height;
    }
}
